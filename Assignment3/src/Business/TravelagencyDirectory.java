/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author enshushang
 */
public class TravelagencyDirectory {
    private ArrayList<Travelagency> traveDirectory;

    public ArrayList<Travelagency> getTraveDirectory() {
        return traveDirectory;
    }

    public void setTraveDirectory(ArrayList<Travelagency> traveDirectory) {
        this.traveDirectory = traveDirectory;
    }

    
    
    public TravelagencyDirectory() {
        traveDirectory = new ArrayList<Travelagency>();
    }
    
    public Travelagency addTravelagency() {
        Travelagency t = new Travelagency();
        traveDirectory.add(t);
        return t;
    }
    
}
