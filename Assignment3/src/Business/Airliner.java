/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author enshushang
 */
public class Airliner {
    private String flightnumber;
    private String date;
    private String fromlocation;
    private String tolocation;
    private String departuretime;
    public int[][] seattable;
    
    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromlocation() {
        return fromlocation;
    }

    public void setFromlocation(String fromlocation) {
        this.fromlocation = fromlocation;
    }

    public String getTolocation() {
        return tolocation;
    }

    public void setTolocation(String tolocation) {
        this.tolocation = tolocation;
    }

    public String getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(String departuretime) {
        this.departuretime = departuretime;
    }

    public int[][] getSeattable() {
        return seattable;
    }

   public void SetSeatTable(){
   int index = 0;
        int[][] arr = new int[150][3];
        for (int i = 1; i <= 25 ; i++) {
            for (int j = 1; j <= 6; j++) {
                arr[index][0] = i;
                arr[index][1] = j;
                arr[index][2] = 1;
                index++;
            }
        }
   
   this.seattable=arr;
   }

    public Airliner() {
        SetSeatTable();
    }
   
   
    
        @Override
    public String toString() {
        return this.getFlightnumber();
    }    
    public boolean isAvailable(int row , int column,int[][] arr){
	for (int i = 0; i < 150; i++) {
            if(row==arr[i][0] && column==arr[i][1] && arr[i][2]==1){
		return true;
            }else if(row==arr[i][0] && column==arr[i][1] && arr[i][2]==0){
		return false;
            }
	}
            return false;
	}
    public void book(int a , int b,int[][] arr){
        for (int i = 0; i < 150; i++) {
            if(a==arr[i][0] && b==arr[i][1]){
                //找到那个座位,修改他的第三列 也就是 Available 这个值 
                arr[i][2]=0;
            }
        }
    }
}
