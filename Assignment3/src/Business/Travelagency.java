/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author enshushang
 */
public class Travelagency {
    private String Company;
    private String flightamount;
    private String flightsize;

    public String getCompany() {
        return Company;
    }

    public void setCompany(String Company) {
        this.Company = Company;
    }

    public String getFlightamount() {
        return flightamount;
    }

    public void setFlightamount(String flightamount) {
        this.flightamount = flightamount;
    }

    public String getFlightsize() {
        return flightsize;
    }

    public void setFlightsize(String flightsize) {
        this.flightsize = flightsize;
    }
    
   @Override
    public String toString() {
        return this.getCompany();
    }    
        
    
    
}
