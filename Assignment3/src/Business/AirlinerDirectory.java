/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author enshushang
 */
public class AirlinerDirectory {
    private ArrayList<Airliner> airlineDirectory;
    
    public AirlinerDirectory(){
        airlineDirectory = new ArrayList<Airliner>();
    }

    public ArrayList<Airliner> getAirlineDirectory() {
        return airlineDirectory;
    }

    public void setAirlineDirectory(ArrayList<Airliner> airlineDirectory) {
        this.airlineDirectory = airlineDirectory;
    }
    public Airliner addAirliner(){
        Airliner al = new Airliner();
        airlineDirectory.add(al);
        return al;
}
     public void deleteAirliner(Airliner airliner){
        airlineDirectory.remove(airliner);

}



    public Airliner searchAccount(String search) {
        
        for(Airliner a:this.getAirlineDirectory()){
    
            if( a.getFlightnumber().equalsIgnoreCase(search)){
                return a;
            }

            if(a.getDate().equalsIgnoreCase(search)){
                return a;
            }

            if(a.getFromlocation().equalsIgnoreCase(search)){
                return a;
            }

            if(a.getTolocation().equalsIgnoreCase(search)){
                return a;
            }

            if(a.getDeparturetime().equalsIgnoreCase(search)){
                return a;
            }
            
        
    
        }
        return null;
    }
    
    
}
